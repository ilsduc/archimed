#!/bin/bash

apachectl -D FOREGROUND

for filename in /etc/apache2/sites-available/*;
  do
    a2ensite $(basename $filename);
  done

service apache2 reload
