import ArchimedDOM from '../../modules/archimed-dom/index.js';
import Main from './Main.js';

/**
 * Inject 
 */
ArchimedDOM.render(
    new Main,
    document.getElementById('root')
);