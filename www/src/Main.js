import Archimed from '../../modules/archimed/index.js';
import Component from '../modules/archimed/Component.js';
import { Router } from '../modules/archimed-dom/navigation.js';

import Header from './components/Header.js';
import Home from './pages/Home.js';
import RouterPage from './pages/Router.js';
import HandleState from './pages/HandleState.js';
import Footer from './components/Footer.js';

const styles = {
    main_container: {
        minHeight: '100vh', 
        display: 'flex', 
        flexDirection: 'column'
    },
    router_container: {
        flex: 1,
    },
};

class Main extends Component {

    render () {
        return Archimed.createElement('div', {id: 'main', style: styles.main_container},
            new Header,
            Archimed.createElement('div', {style: styles.router_container},
                new Router({
                    routes: [
                        {
                            path: '/', component: new Home, default: true,
                        },
                        {
                            path: '/routing', component: new RouterPage,
                        },
                        {
                            path: '/state', component: new HandleState,
                        },
                    ]
                })
            ),
            new Footer
        )
    }
}


export default Main;