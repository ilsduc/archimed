import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import Brand from './Brand.js';

const styles = {
    footer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'rgb(30, 30, 30)',
        boxShadow: '0 2px 7px rgba(0, 0, 0, .25)',
        padding: '1rem 1rem',
        color: 'white',
        fontSize: '.9rem',
    },
    authors: {
        margin: '.5rem',
    }
};

class Footer extends Component {
    render () {
        return Archimed.createElement('footer', {id: 'header', style: styles.footer},
            new Brand,
            Archimed.createElement('div', {style: styles.authors}, '© 2020 Copyright Archimed.js - Ilan Ducret & Charles Van Hamme')
        );
    }
}

export default Footer;
