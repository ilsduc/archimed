import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';

const styles = {
    code_container: {
        background: 'rgb(30, 30, 30)',
        color: 'rgb(2335, 225, 225)',
        overflowX: 'scroll',
        padding: '2rem',
        borderRadius: '3px',
        textAlign: 'left',
        marginBottom: '1rem',
        borderLeft: '4px solid #4287f5',
    },
    code: {

    }
}

class Code extends Component {
    constructor (props) {
        super(props);
    }
    render () {
        return Archimed.createElement('div', {style: styles.code_container},
            Archimed.createElement('code', {style: styles.code},
                Archimed.createElement('pre', null,
                    this.props.label||''
                )
            )
        )
    }
}

export default Code;