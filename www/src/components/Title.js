import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';

const styles = {
    title: {
        color: 'rgb(30, 30, 30)',
        borderRadius: '3px',
        textAlign: 'left',
        fontSize: '1.5rem',
        fontWeight: 'bold',
        marginTop: '2rem',
    }
}

class Title extends Component {
    constructor (props) {
        super(props);
    }
    render () {
        return Archimed.createElement('p', {style: styles.title},
            this.props.label||''
        )
    }
}

export default Title;