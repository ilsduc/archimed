import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';

const styles = {
    counter: {
        wdith: '100%',
        marginTop: '2rem',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    counterInfo: {
        padding: '1rem',
        margin: '.5rem',
        backgroundColor: 'rgb(30, 30, 30)',
        color: 'white',
        fontSize: '1.5rem',
        borderRadius: '5px',
    },
    button: {
        padding: '.5rem 1rem',
        border: '2px solid rgb(30, 30, 30)',
        borderRadius: '5px',
        backgroundColor: 'transparent',
        color: 'rgb(30, 30, 30)',
        textTransform: 'uppercase',
    }
};

class Counter extends Component {
    constructor (props) {
        super(props);

        this.state = {
            counter: Math.ceil(Math.random(0, 1000) * 100),
        };
    }

    increment = () => this.setState({ counter: this.state.counter + 1 });

    decrement = () => this.setState({ counter: this.state.counter - 1 });

    render () {
        return Archimed.createElement('div', {style: styles.counter},
            Archimed.createElement('button', { onClick: () => this.decrement(), style: styles.button }, 'decrement'),
            Archimed.createElement('p', {style: styles.counterInfo}, 
                this.state.interpolate('{{ counter }}') + ''
            ),
            Archimed.createElement('button', { onClick: () => this.increment(), style: styles.button }, 'increment'),
        );
    }
}

export default Counter;