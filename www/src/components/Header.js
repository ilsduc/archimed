import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import { Link } from '../../modules/archimed-dom/navigation.js';
import Brand from './Brand.js';

const styles = {
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        background: 'rgb(30, 30, 30)',
        boxShadow: '0 2px 7px rgba(0, 0, 0, .25)',
        padding: '0 1rem',
    },
    ul: {
        padding: 0,
        margin: 0,
        display: 'flex',
        alignItems: 'center',
    },
    li: {
        display: 'inline-block',
    },
    logo: {
        width: '30px',
        height: 'auto',
    },
    a: {
        display: 'block',
        padding: '1rem',
        color: 'white',
        textDecoration: 'none',
    },
    brand: {
        display: 'flex',
        alignItems: 'center',
        textDecoration: 'none',
        color: 'white',
    }
};

class Header extends Component {
    render () {
        return Archimed.createElement('header', {id: 'header', style: styles.header},
            Archimed.createElement('a', {href: '/', style: styles.brand},
                Archimed.createElement('img', {src: './src/assets/logocolored.svg', style: styles.logo}, null),
                new Brand,
            ),
            Archimed.createElement('ul',{id: 'menu-list', style: styles.ul},
                Archimed.createElement('li',{class: 'menu-list-item', style: styles.li},
                    Link({href: '/', label: 'Archimed.js', style: styles.a})
                ),
                Archimed.createElement('li',{class: 'menu-list-item', style: styles.li},
                    Link({href: '/routing', label: 'Routing', style: styles.a})
                ),
                Archimed.createElement('li',{class: 'menu-list-item', style: styles.li},
                    Link({href: '/state', label: 'Handle state', style: styles.a})
                ),
                Archimed.createElement('li',{class: 'menu-list-item', style: styles.li},
                    Archimed.createElement('a',{href: 'https://gitlab.com/ilsduc/archimed', target: '_bank', style: styles.a},
                        Archimed.createElement('img', { src: './src/assets/gitlab.svg', width: '30px', height: 'auto' }, null),
                    ),
                ),
            )
        );
    }
}

export default Header;