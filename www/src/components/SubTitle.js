import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';

const styles = {
    title: {
        color: 'rgb(70, 70, 70)',
        borderRadius: '3px',
        textAlign: 'left',
        marginTop: '2rem',
    }
}

class SubTitle extends Component {
    constructor (props) {
        super(props);
    }
    render () {
        return Archimed.createElement('p', {style: styles.title},
            this.props.label||''
        )
    }
}

export default SubTitle;