import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import { types } from '../../modules/archimed/dom/typeChecker.js';

const styles = {
    brand: {
        fontFamily: '\'Euphoria Script\', cursive',
        fontSize: '2rem',
        margin: '.5rem 1rem',
        color: '#4287f5',
    },
};

class Brand extends Component {

    requiredProps = {
        children: types.object, 
    };

    render () {
        return Archimed.createElement('h2', {style: styles.brand}, 'Archimed.js');
    }
}

export default Brand;

