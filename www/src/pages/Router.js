import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import Code from '../components/Code.js';
import SubTitle from '../components/SubTitle.js';
import Title from '../components/Title.js';

const styles = {
    container: {
        maxWidth: '980px',
        margin: 'auto',
        textAlign: 'center',
    },
};

class Router extends Component {
    render () {
        return Archimed.createElement('div', {id: 'home-page', style: styles.container},
            new Title({label: 'The router Component'}),
            new SubTitle({label: 'The router allows you to display an Archimed Component depending on the location pathname'}),
            new Code({label: 
                'ArchimedDOM.render(\n' +
                '\tnew Router({\n' +
                '\t\troutes: [\n'+
                '\t\t\t{\n'+
                '\t\t\t\tpath: \'/\', component: \'My default page.\', default: true,\n'+
                '\t\t\t},\n'+
                '\t\t\t{\n'+
                '\t\t\t\tpath: \'/awesome-feature\', component: new AwesomeFeature,\n'+
                '\t\t\t},\n'+
                '\t\t]\n'+
                '\t}),\n'+
                '\tdocument.getElementById(\'root\')\n'+
                ');'
            }),
            new Title({label: 'The Navigator API'}),
            new SubTitle({label: 'The Navigator object gives you a way to navigate into Archimed routing.'}),
            new Code({label: 
                'import { Navigator } from \'my/path/to/navigation.js\'\n' +
                '\n' +
                'class MyAwesomeButton extends Component {\n' +
                '\tconstructor(props) {\n'+
                '\t\tsuper(props);\n' + 
                '\t}\n' + 
                '\n' + 
                '\tonClick = () => Navigator.navigate(\'/awesome-feature\');\n' + 
                '\n' + 
                '\trender() {\n' + 
                '\t\treturn Archimed.createElement(\n'+
                '\t\t\t\'button\', \n' +
                '\t\t\t{onClick: () => this.onClick()},\n'+
                '\t\t\t\'See this awesome feature\'\n'+
                '\t\t);\n' + 
                '\t}\n' + 
                '}\n'
            }),
            new Title({label: 'The Link Component'}),
            new SubTitle({label: 'The Link Component is a shortcut. That uses Archimed routing.'}),
            new Code({label: 
                'import { Link } from \'my/path/to/navigation.js\'\n' +
                '\n' +
                'class MyAwesomeMenu extends Component {\n' +
                '\tconstructor(props) {\n'+
                '\t\tsuper(props);\n' + 
                '\t}\n' + 
                '\n' + 
                '\trender() {\n' + 
                '\t\treturn Archimed.createElement(\'ul\', null ,\n' +
                '\t\t\tArchimed.createElement(\'li\', null,\n' +
                '\t\t\t\tLink({href: \'/link-1\', label: \'Link 1\'})\n' +
                '\t\t\t),\n' +
                '\t\t\tArchimed.createElement(\'li\', null,\n' +
                '\t\t\t\tLink({href: \'/link-2\', label: \'Link 2\'})\n' +
                '\t\t\t),\n' +
                '\t\t\tArchimed.createElement(\'li\', null,\n' +
                '\t\t\t\tLink({href: \'/link-3\', label: \'Link 3\'})\n' +
                '\t\t\t),\n' +
                '\t\t);\n' +
                '\t}\n' + 
                '}\n'
            })
        );
    }
}

export default Router;

