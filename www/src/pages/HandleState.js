import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import Code from '../components/Code.js';
import SubTitle from '../components/SubTitle.js';
import Title from '../components/Title.js';
import Counter from '../components/Counter.js';

const styles = {
    container: {
        maxWidth: '980px',
        margin: 'auto',
        textAlign: 'center',
    },
};

class HandleState extends Component {
    render () {
        return Archimed.createElement('div', {id: 'handle-page', style: styles.container},
            new Title({label: "A little but efficient state"}),
            new SubTitle({label: "Manage a state as like you were a React developper."}),
            new Code({label: 
                'class MyArchimedComponent extends Component {\n' +
                '\tconstructor(props) {\n'+
                '\t\tsuper(props);\n' + 
                '\t\tthis.state = {\n' + 
                '\t\t\tcounter: 0,\n' + 
                '\t\t}\n' + 
                '\t}\n' + 
                '\n' + 
                '\tcomponentDidMount() {\n' + 
                '\t\tthis.setState({ counter: Math.random(0, 100) });\n' + 
                '\t}\n' + 
                '\n' + 
                '\tincrement = () => this.setState({ counter: this.state.counter + 1 });\n' + 
                '\n' + 
                '\tdecrement = () => this.setState({ counter: this.state.counter - 1 });\n' + 
                '\n' + 
                '\trender() {\n' + 
                '\t\treturn Archimed.createElement(\'div\', null,\n' +
                '\t\t\tArchimed.createElement(\'button\', {onClick: this.increment()}, \'increment\'),\n' +
                '\t\t\tArchimed.createElement(\'p\', null, this.state.interpolate(\'{{ counter }}\')),\n' +
                '\t\t\tArchimed.createElement(\'button\', {onClick: this.decrement()}, \'decrement\'),\n' +
                '\t\t);\n' + 
                '\t}\n' + 
                '}\n' 
            }),
            new Counter
        );
    }
}

export default HandleState;