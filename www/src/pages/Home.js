import Archimed from '../../modules/archimed/index.js';
import Component from '../../modules/archimed/Component.js';
import Code from '../components/Code.js';
import SubTitle from '../components/SubTitle.js';
import Title from '../components/Title.js';

const styles = {
    container: {
        maxWidth: '980px',
        margin: 'auto',
        textAlign: 'center',
    },
};

class Home extends Component {
    render () {
        return Archimed.createElement('div', {id: 'home-page', style: styles.container},
            new Title({label: "The base element"}),
            new SubTitle({label: "Create a simple virtualDOM element. This will create a virtual div with an id #foobar and a text within."}),
            new Code({label: 
                'Archimed.createElement(\'div\', {id: \'foobar\'}, \'Hello World!\');'
            }),
            new Title({label: "The Component"}),
            new SubTitle({label: "A Component must have a render method that returns an Archimed Element. He has the advantage of being able to manage his state."}),
            new Code({label: 
                'class MyArchimedComponent extends Component {\n' +
                '\tconstructor(props) {\n'+
                '\t\tsuper(props);\n' + 
                '\t}\n' + 
                '\n' + 
                '\trender() {\n' + 
                '\t\treturn Archimed.createElement(\'div\', {id: \'foo\'}, \'Hello from MyArchimedComponent\');\n' + 
                '\t}\n' + 
                '}\n' 
            }),
            new Title({label: "Inject to the DOM"}),
            new SubTitle({label: "ArchimedDOM lets you inject an ArchimedElement, with its children into a DOM Element."}),
            new Code({label: 
                'ArchimedDOM.render(\n' +
                    '\tnew MyArchimedComponent(),\n'+
                    '\tdocument.getElementById(\'root\')\n'+
                ');'
            })
        );
    }
}

export default Home;