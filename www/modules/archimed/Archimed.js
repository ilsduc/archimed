import './helpers/helpers.js';
import { createChildElement } from './vdom.js';

const Archimed = {
    createElement: (tag, props, ...children) => ({
        type: tag !== '' ? 'NODE_ELEMENT' : 'GHOST_ELEMENT',
        tag,
        props,
        node_reference: Math.random(0, 10000000),
        node: document.createElement(tag),
        children: children.map(child => createChildElement(child)),
    }),
};

 export default Archimed;