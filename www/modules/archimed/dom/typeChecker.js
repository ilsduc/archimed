export const types = {
    object: 'object',
    function: 'function',
    string: 'string',
    number: 'number',
    boolean: 'boolean',
}

Object.prototype.checkTypes = function (requiredProps) {
    for (let property in requiredProps) {
        if (requiredProps.hasOwnProperty(property)) {
            if (this.hasOwnProperty(property)) {
                // 
                if (typeof requiredProps[property] === 'object') {
                    this[property].checkTypes(requiredProps[property]);
                    continue;
                }
                // type verification
                if (!(requiredProps[property] === typeof this[property])) {
                    throw property + ' should be type of ' + requiredProps[property] + ' and it\'s type is '+(typeof this[property]);
                } 
            } else {
                throw property + ' is marked as required and cannot be found.';
            }
        }
    }
}
