/**
 *
 * @param type
 * @param props
 * @param children
 */
export function createElement(type, props, ...children) {
    return {
        type,
        props: {
            ...props,
            children: children.map(child => {
                return typeof child === 'object' ? child : createTextElement(child);
            })
        },
    };
}

/**
 *
 *  Create a node that doesnt have any Children
 * @param text
 * @returns {{type: string, props: {nodeValue: *, children: []}}}
 */
function createTextElement(text) {
    return {
        type: 'TEXT_ELEMENT',
        props: {
            nodeValue: text,
            children: [],
        }  ,
    };
}



