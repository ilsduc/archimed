
/**
 * Add prop_access method to Object prototype
 */
Object.prototype.prop_access = function(properties) {
    let splitedProperties = properties.split(".");
    let currentValue = this;
    let path = "";

    for (let property of splitedProperties) {
        if (currentValue[property]) {
            currentValue = currentValue[property];
            splitedProperties = splitedProperties.slice(1);
            path += property + ".";
            continue;
        } else {
            console.error(
                "Property " + property + " does not exist in " + path.slice(0, -1)
            );
            return undefined;
        }
    }
    return currentValue;
};

Object.prototype.interpolate = function( string ) {
    return string.replace(/{{\s*(\S+)\s*}}/g, (fullmatch, group1) => {
        return this.prop_access(group1);
    });
};

Object.prototype.forEach = function( callback ) {
    let object = {};
    for (let key in this ) {
        if (this.hasOwnProperty(key)) {
            callback(this[key], key);
        }
    }
    return this;
}

Object.prototype.map = function( callback ) {
    let object = {};
    for (let key in this ) {
        if (this.hasOwnProperty(key)) {
            object[key] = callback(this[key], key);
        }
    }
    return object;
}


String.prototype.cssPropertyCase = function() {
    let str = '';
    for (var i = 0; i < this.length; i++) {
        if (this.charCodeAt(i) >= 65 && this.charCodeAt(i) <= 95) {
            str += '-'+this.charAt(i).toLowerCase();
        } else {
            str += this.charAt(i);
        }
      }
      return str;
}