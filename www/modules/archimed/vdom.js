import Component from './Component.js';

export function createChildElement(child) {
    if (child instanceof Component) {
        if (!child.render)  {
            throw 'Component must have a render method.';
        } 
        
        return child.__proto__.__proto__.render(child);
    }

    if (! child ) return null;

    const { type, tag, props, children, node } = child;
    let node_reference = Math.random(0, 10000000);
    return  typeof child === 'object' 
        ? {type: 'NODE_ELEMENT', tag, props, children: children, node_reference, node}
        : {type: 'TEXT_ELEMENT', value: child||'', node_reference };
}
