import { createDOMElement } from './dom.js';
import Component from '../archimed/Component.js';
/**
 * ArchimedDOM Object
 */
class ArchimedDOM  {
    rootVDOM = {};
    rootElement = null;
    /**
     *  Render all content in HTML
     */
    render = (rootVDOM, rootElement) => {
        
        if (rootVDOM instanceof Component) {
            rootVDOM = rootVDOM.__proto__.__proto__.render(rootVDOM);
        }
        this.vdom = rootVDOM;
        this.rootElement = rootElement;
        let rootDOMElement = createDOMElement(this.vdom);
        if (this.rootElement.childNodes.length > 0) {
            this.rootElement.replaceChild(rootDOMElement, this.rootElement.childNodes[0]);
        } else {
            this.rootElement.append(rootDOMElement);
        }
    };

    saveNode(reference, newNode) {
        let vdom = this.recursiveFindReference(this.vdom, reference, newNode);
        this.vdom = vdom;
    }

    forceUpdate() {
        let rootDOMElement = createDOMElement(this.vdom);
        if (this.rootElement.childNodes.length > 0) {
            this.rootElement.replaceChild(rootDOMElement, this.rootElement.childNodes[0]);
        } else {
            this.rootElement.append(rootDOMElement);
        }
    }

    recursiveFindReference (vdom, reference, newValue) {
        let newDom = vdom;
        if (newDom.node_reference === reference) {
            // vdom = newValue;
            return newValue;
        };

        if (vdom.children) {
            for( let key in vdom.children ) {
                if (vdom.children[key] && vdom.children[key].node_reference === reference ) {
                    vdom.children[key] = newValue;
                };
                
                if (vdom.children[key] && vdom.children[key].children && vdom.children[key].children.length > 0) {
                    vdom.children[key] =  this.recursiveFindReference(vdom.children[key], reference, newValue);
                }
            }   
        }

        return vdom;
    }

    modifiyValue(domObject, reference, newValue)
    {

    }

};

export default new ArchimedDOM();