export function createDOMElement(vdom_node) {
    //
    if (!vdom_node) return '';
    //
    if (vdom_node.type === 'NODE_ELEMENT') {
        var element = vdom_node.node;

        if (vdom_node.children) vdom_node.children.map(child => {
            // Append children
            element.append(createDOMElement(child));
        });

        // if (vdom_node.props) vdom_node.props.keys().map(prop => element.setAttribute(prop, vdom_node.props[prop]));
        if (vdom_node.props) {
            vdom_node.props.forEach((prop, key) => {
                if (key.indexOf('on') === 0) {
                    element.addEventListener(key.slice(2).toLowerCase(), prop);
                } else if (key === 'style') {
                    element.setAttribute(key, createInlineStyle(prop));
                } else {
                    element.setAttribute(key, prop);
                }
            });
        }
        return element;
    }

    if (vdom_node.type == 'TEXT_ELEMENT') {
        return vdom_node.value;
    } 
}

function createInlineStyle(style) {
    let inlineStyle = '';

    style.forEach((prop, key) => {
        inlineStyle += key.cssPropertyCase() + ': ' + prop + '; ';
    });

    return inlineStyle;
}

