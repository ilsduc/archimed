import Archimed from "../archimed/Archimed.js";
import Component from '../archimed/Component.js';

export class Router extends Component {
    navigate = this.navigatation;
    constructor(props) {
        super(props);
        this.state = {
            currentRoute: this.create(props.routes)
        };

        window.onpopstate = (event) => {
            this.setState({
                currentRoute: this.create(props.routes),
            });
        };
    }

    componentDidMount() {
        this.create(this.state.currentRoute);
    }

    create = (routes) => {

        let matchedRoute = null;
        let find = false;
        this.routes = routes;
        let router = routes.forEach(route => {
            route = this.match(route);
            if ((route.match === true || route.default === true) && !find) {
                matchedRoute = route.component;
            }
        });
        
        this.current_route = Archimed.createElement('div', {id: 'route-match'}, matchedRoute);
        
        return this.current_route;
    }

    match = (route) => (route.path === window.location.pathname ? { 
        match: true,
        pathname: window.location.pathname,
        component: route.component,
        default: route.default,
    }
    : {
        match: false,
        default: route.default,
        component: route.default ? route.component : null,
    });

    render () {
        return this.state.currentRoute;
    }
} 

export const Navigator = {
    navigate: (path) => {
        let wantedSlug = path.replace(/^http(s)?:\/\/(.*)\//g, '');
        let currentSlug = window.location.pathname.replace(/^\/+|\/+$/gm,'');
        if (wantedSlug === currentSlug) return;
        window.history.pushState(null, document.title, path);
        var popStateEvent = new PopStateEvent('popstate', null);
        dispatchEvent(popStateEvent);
    }
}

export const Link = (props) => {

    const onClick = (e) => {
        e.preventDefault();

        Navigator.navigate(e.target.href);
    }

    return Archimed.createElement(
        'a',
        {
            onClick : (e) => onClick(e),
            ...props,
        },
        props.label || props.children || null,
    );
}