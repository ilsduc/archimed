# Javascript Library for reactive programmation.

## Live demo
See the live demo here [archimed.ilsduc.fr](http://archimed.ilsduc.fr)!

## Setup
Run the following command, and then open your favorite browser on http://localhost

```
docker-compose up -d
```

## Interpolate
You can find in `/modules/archimed/helpers/helpers.js` Object.prototype.interpolate( string )
```
// example

myObject = {
    counter: 10,
};

myObject.interpolate('{{ counter }}'); // 10
```

## PropTypes
You can find in `/modules/archimed/dom/typeChecker.js` Object.prototype.checkTypes( configs )
```
// example

import { types } from './path/to/types';

const User = {
    name: 'Ilan',
    age: 13,
    greets: () => console.log('Hello Karl!'),
    car: {
       color: 'red',
    },
};

const propTypes = {
    name: types.string,
    age: types.number,
    greets: types.function,
    car: {                      // Implicity verifiying that car is an object
        color: types.string,
    },
};

// Let's verify
User.checkTypes( propTypes );
```

:mortar_board: :computer:

